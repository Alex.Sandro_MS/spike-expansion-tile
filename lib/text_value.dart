import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class TextValue extends Align {
  TextValue(String? data)
      : super(
            alignment: Alignment.centerLeft,
            child: Text(textUtilsValidatesContentText(data),
                overflow: TextOverflow.fade,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: ProjectDimens.getFontValue,
                    color: Colors.black)));
}

///Classe criada para permitir quebras de linhas em widgets dentro de Rows e Colunms
class TextValueWithFlexible extends Flexible {
  TextValueWithFlexible(String? data)
      : super(
            child: Align(
            alignment: Alignment.centerLeft,
            child: Text(textUtilsValidatesContentText(data),
                overflow: TextOverflow.fade,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: ProjectDimens.getFontValue,
                    color: Colors.black))));
}

class TextValueOrShimmer extends Align {
  TextValueOrShimmer(String? data, {bool? isLarge})
      : super(
            alignment: Alignment.centerLeft,
            child: data == null
                ? TextShimmer(isLarge: isLarge == true)
                : Text(textUtilsValidatesContentText(data),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ProjectDimens.getFontValue,
                        color: Colors.black)));
}

String textUtilsValidatesContentText(String? text) {
  if (text != null && text.isNotEmpty) {
    return text;
  }
  return 'Não Informado';
}

class ProjectDimens {
  final double _fontNormal = 20.0;
  final double _fontBig = 30.0;

  static final double _emptySpaceVerticalNormal = 20.0;

  ///usar metodo estatico
  @Deprecated('Usar metodo estatico [getFontValue]')
  double getFontNormal() {
    return _fontNormal;
  }

  ///usar metodo estatico
  @Deprecated('Usar metodo estatico [getFontLabel]')
  double getFontBig() {
    return _fontBig;
  }

  static double getEmptySpaceVerticalNormal() {
    return _emptySpaceVerticalNormal;
  }

  static double getFontLabel = 16.0;
  static double getFontValue = 20.0;
}


class TextShimmer extends StatelessWidget implements TextShimmerBase {
  final bool isLarge;
  const TextShimmer({
    Key? key,
    this.isLarge = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300]!,
        highlightColor: Colors.grey[100]!,
        child: Container(
          height: 25,
          width: isLarge ? MediaQuery.of(context).size.width * 0.6 : MediaQuery.of(context).size.width/3,
          color: Colors.white,
        ),
      ),
    );
  }
}

class TextShimmerFlexible extends StatelessWidget implements TextShimmerBase {
  const TextShimmerFlexible({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300]!,
          highlightColor: Colors.grey[100]!,
          child: Container(
            height: 25,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

abstract class TextShimmerBase extends Widget {}