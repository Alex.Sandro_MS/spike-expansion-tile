import 'package:flutter/material.dart';

///Adapter utiilizado para dizer a lista do home como criar o tile
///O Adapter tem um parametro genérico referente a lista
abstract class AdapterTile<T>{
  Widget createTile(List<T> list, int index);
}