import 'package:flutter/material.dart';
import 'package:spike_expansion_tile/text_value.dart';

class TextLabel extends Align {
  TextLabel(String? data)
      : super(
            alignment: Alignment.centerLeft,
            child: Text(textUtilsValidatesContentText(data),
                style: TextStyle(
                    fontSize: ProjectDimens.getFontLabel,
                    color: Colors.black)));
}

///Classe criada para permitir quebras de linhas em widgets dentro de Rows e Colunms

class TextLabelWithFlexible extends Flexible {
  TextLabelWithFlexible(String? data)
      : super(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(textUtilsValidatesContentText(data),
            style: TextStyle(
                fontSize: ProjectDimens.getFontLabel,
                color: Colors.black)),
      ));
}
