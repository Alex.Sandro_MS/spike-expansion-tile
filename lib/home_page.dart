import 'package:flutter/material.dart';
import 'package:spike_expansion_tile/adapter_tile.dart';
import 'package:spike_expansion_tile/custom_expansion_tile.dart';
import 'package:spike_expansion_tile/recognicao_tile.dart';

import 'foto_pessoas_tile.dart';

class HomePage extends StatefulWidget{
  ///As listas de upload estão aqui representadas por valores inteiros
  final listFotosPessoa = [5, 4, 9, 1, 3];
  final listUploadRecognicao = [1, 8, 7, 2, 2, 3 ];

  HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ValueNotifier<Key?> _expanded = ValueNotifier(null);
  late double heightScreen;

  @override
  Widget build(BuildContext context) {
    heightScreen = MediaQuery.of(context).size.height;

    return ListView(
      children: <Widget>[
        CustomExpansionTile(
          expandedItem: _expanded,
            ///Importante definir keys com valores diferentes
          key: const Key('1'),
            ///Passar o Título e o tamanho da lista aqui
          title: buildTitle('Pessoa', widget.listFotosPessoa.length.toString()),
            ///Passar a Lista de upload aqui, a porcentagem de altura do card e o Adapter do tile
            //0.23
            //
          child: getList(list: widget.listFotosPessoa, percentHeightScreen: 0.25, adapterTile: FotoPessoaTileAdapter())
        ),

        CustomExpansionTile(
          expandedItem: _expanded,
          ///Importante definir keys com valores diferentes
          key: const Key('2'),
          ///Passar o Título e o tamanho da lista aqui
          title: buildTitle('Recognição', widget.listUploadRecognicao.length.toString()),
          ///Passar a Lista de upload aqui, a porcentagem de altura do card e o Adapter do tile
            //0.29
            //
          child: getList(list: widget.listUploadRecognicao, percentHeightScreen: 0.31, adapterTile: RecognicaoTileAdapter())
        ),
      ],
    );
  }

  ///Metodo de criação do titulo passando uma String
  ///A cor pode ser alterada para a cor do projeto
  Row buildTitle(String title, String listLength) {
    const primaryColor = Colors.deepPurple;

    return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 12),
              ///Utilizar a lib para colocar reticências aqui
              child: Text(
                title,
                style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            CircleAvatar(
              backgroundColor: primaryColor,
              child: Text(
                  listLength,
                  style: const TextStyle(color: Colors.white, fontSize: 20)),
            )
          ],
        );
  }

  /// Deve passar a lista de fotos,
  /// o percentual da altura da tela que o card deve ter
  /// e o AdapterTile que mostra como criar o tile correto(Recognição ou Pessoa)
  Widget getList({required List list, required double percentHeightScreen, required AdapterTile adapterTile}){

    return SizedBox(
      height: heightScreen * percentHeightScreen,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: list.length,
        itemBuilder: (_, i) {
          return Padding(
            padding: const EdgeInsets.all(4.0),
            child: adapterTile.createTile(list, i)
          );
        },
      ),
    );
  }
}