import 'package:flutter/material.dart';
import 'package:spike_expansion_tile/adapter_tile.dart';
import 'package:spike_expansion_tile/foto_pessoas_tile.dart';
import 'package:spike_expansion_tile/item_counter_card.dart';
import 'package:spike_expansion_tile/text_label.dart';
import 'package:spike_expansion_tile/text_value.dart';

///Adapter usado para mostrar ao metodo getLista do home como criar o tile
class RecognicaoTileAdapter extends AdapterTile<int>{
  @override
  Widget createTile(List<int> list, int index) {
    return RecognicaoTile(list, index);
  }
}

class RecognicaoTile extends StatelessWidget{
  RecognicaoTile(
      this.listFotos,
      this.index,
      {super.key}
      );
  final int index;
  final List<int> listFotos;

  final Widget spaceBetweenLines = Container(height: 8);

  @override
  Widget build(BuildContext context) {

    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.8,
      child: Card(
          color: const Color(0xFFE6E6EC),
          child: Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  ItemCounterCard(index: index,),
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 8.0, 16.0, 8.0, ),
                      child: TextValue('70° D.P. - VILA EMA'),
                    ),
                  )
                ],
              ),
              spaceBetweenLines,
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            TextLabel('B.O: '),
                            TextValue(
                              'AA8675/2022',
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            TextLabel('Circunsc.: '),

                            TextValue(
                              '70 D.P. - VILA EMA',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),

              Container(
                margin: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  children: [
                    const Expanded(child: WidgetItemRecognicaoStatus(2)),
                    Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: IconTotalFotosItemUpload(numeroFotos: listFotos.length),
                    )
                  ],
                ),
              )
            ],
          )
      ),
    );
  }
}

class WidgetItemRecognicaoStatus extends StatelessWidget {
  final int? _status;

  const WidgetItemRecognicaoStatus(this._status, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.elliptical(5, 10)),
          color: uploadStatusBackgroundColor[
          _status ?? 1]),
      child: Center(
        child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: uploadStatusTextWidget[
            _status ?? 1]),
      ),
    );
  }
}


var uploadStatusTextWidget = {
  1: _getTextWidget('Rascunho', Colors.black),
  2: _getTextWidget('Enviando', Colors.white),
  3: _getTextWidget('Fotos Enviadas', Colors.white),
  4: _getTextWidget('PDF Gerado', Colors.white),
  5: _getTextWidget('Erro', Colors.white),
};

var uploadStatusBackgroundColor = {
  1: Colors.grey[350]!,
  2: Colors.blueGrey,
  3: Colors.grey[500]!,
  4: Colors.deepPurple,
  5: Colors.red,
};

Text _getTextWidget(String text, Color color) {
  return Text(
    text,
    style: TextStyle(color: color, fontWeight: FontWeight.bold),
  );
}

