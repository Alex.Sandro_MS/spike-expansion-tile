import 'package:flutter/material.dart';
import 'package:spike_expansion_tile/adapter_tile.dart';
import 'package:spike_expansion_tile/text_label.dart';
import 'package:spike_expansion_tile/text_value.dart';

import 'item_counter_card.dart';

class FotoPessoaTileAdapter extends AdapterTile<int>{
  @override
  Widget createTile(List<int> list, int index) {
    return FotoPessoaTile(list, index);
  }
}

class FotoPessoaTile extends StatelessWidget{
  FotoPessoaTile(
      this.listFotos,
      this.index,
      {super.key}
  );

  @override
  final int index;
  @override
  final List<int> listFotos;

  final Widget spaceBetweenLines = Container(height: 8);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.8,
      child: Card(
        color: const Color(0xFFE6E6EC),
        child: Column(
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                ItemCounterCard(index: index,),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 8.0, 16.0, 8.0, ),
                    child: TextValue('João da Silva'),
                  ),
                )
              ],
            ),
            spaceBetweenLines,
            Container(
              margin: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0, ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          TextLabel('RG: '),
                          TextValue(
                            '12345678',
                          ),
                        ],
                      ),
                      TextLabel('Quinta - 10:22'),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 12.0),
                    child: IconTotalFotosItemUpload(numeroFotos: listFotos.length),
                  )
                ],
              ),
            ),
          ],
        )
      ),
    );
  }
}

class IconTotalFotosItemUpload extends StatelessWidget {
  const IconTotalFotosItemUpload({
    Key? key,
    required this.numeroFotos,
  }) : super(key: key);

  final int numeroFotos;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const SizedBox(
          height: 80,
          width: 80,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(
                Icons.photo_library_rounded,
                color: Colors.black54,
                size: 40,
            ),
          ),
        ),
        Positioned(
          top: 45,
          left: 45,
          child: CircleAvatar(
            maxRadius: 10,
            backgroundColor: Colors.white,
            child: Text(
              textUtilsValidatesContentText(numeroFotos.toString()),
              style: TextStyle(color: Colors.black87),
            ),
          ),
        )
      ],
    );
  }
}
