import 'package:flutter/material.dart';

///
///Author: Alex Sandro
///Email: ASandro@MagnaSistemas.com
///
class ItemCounterCard extends StatelessWidget {
  const ItemCounterCard({
    Key? key,
    int? index,
  })  : _index = index,
        super(key: key);

  final int? _index;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 0, 8, 0),
      child: Card(
        color: Colors.black54,
        elevation: 3.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            _index == null ? '-' : (_index! + 1).toString(),
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w900, fontSize: 18),
          ),
        ),
      ),
    );
  }
}
